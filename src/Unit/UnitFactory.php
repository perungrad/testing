<?php

namespace Workshop\Testing\Unit;

use Workshop\Testing\Unit\Unit;

class UnitFactory
{

    /**
     * @return Unit
     */
    public function create()
    {
        $unit = new Unit();

        return $unit;
    }

    /**
     * @param array $data
     *
     * @return Unit
     */
    public function createFromData($data)
    {
        $keys = [
            'id',
            'name',
            'type',
            'attack',
            'defense',
            'speed',
        ];

        $unit = $this->create();

        foreach ($keys as $key) {
            if (array_key_exists($key, $data)) {
                $methodName = 'set' . $key;

                if (method_exists($unit, $methodName)) {
                    $unit->$methodName($data[$key]);
                }
            }
        }

        return $unit;
    }
}

