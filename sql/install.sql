create table if not exists unit(
    id int unsigned not null auto_increment primary key,
    name varchar(100) default '' collate utf8mb4_unicode_ci,
    type int unsigned default null,
    attack tinyint unsigned default 10,
    defense tinyint unsigned default 10,
    speed decimal(5,2),
    unique (name),
    index (type, name, attack, defense, speed),
    index (name, type, attack, defense, speed)
) engine=innodb default charset=utf8mb4 collate=utf8mb4_unicode_ci;

insert into unit
    (name, type, attack, defense, speed)
values
    ('swordsman', 1, 9, 8, 1),
    ('pikeman', 1, 10, 11, 1),
    ('archer', 2, 10, 5, 1),
    ('crossbowman', 2, 12, 4, 1),
    ('longbow-archer', 2, 12, 6, 1),
    ('light-cavalry', 3, 8, 10, 3),
    ('heavy-cavalry', 3, 11, 12, 2),
    ('knight', 3, 12, 12, 2),
    ('catapult', 4, 15, 8, 1),
    ('siege-tower', 4, 18, 14, 1);
