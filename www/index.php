<?php

use Dibi\Connection;
use Latte\Engine;
use Workshop\Testing\Unit\UnitRepository;
use Workshop\Testing\Unit\UnitFactory;
use Workshop\Testing\Unit\UnitFacade;

require __DIR__ . '/../vendor/autoload.php';

$rootDir     = realpath(__DIR__ . '/..');
$tmpDir      = $rootDir . '/tmp';
$templateDir = $rootDir . '/templates';

$connection = new Connection([
    'driver'   => 'mysql',
    'host'     => 'localhost',
    'user'     => 'workshop',
    'password' => 'workshop',
    'database' => 'workshop_testing',
]);

$unitRepository = new UnitRepository($connection);
$unitFactory    = new UnitFactory();
$unitFacade     = new UnitFacade($unitRepository, $unitFactory);

$latte = new Engine();
$latte->setTempDirectory($tmpDir . '/latte');

$latte->render(
    $templateDir . '/index.latte',
    [
        'title' => 'Homepage',
        'units' => $unitFacade->getUnits(),
    ]
);
