<?php

namespace Workshop\Testing\Unit;

use Workshop\Testing\Unit\UnitRepository;
use Workshop\Testing\Unit\UnitFactory;

class UnitFacade
{
    /** @var UnitRepository */
    private $unitRepository;

    /** @var UnitFactory */
    private $unitFactory;

    /**
     * @param UnitRepository $unitRepository
     * @param UnitFactory    $unitFactory
     */
    public function __construct(UnitRepository $unitRepository, UnitFactory $unitFactory)
    {
        $this->unitRepository = $unitRepository;
        $this->unitFactory    = $unitFactory;
    }

    /**
     * @return array<Unit>
     */
    public function getUnits()
    {
        $units = [];

        $rows = $this->unitRepository->findAll();

        foreach ($rows as $row) {
            $unit = $this->unitFactory->createFromData($row);

            $units[$unit->getId()] = $unit;
        }

        return $units;
    }
}

