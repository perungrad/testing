<?php

namespace Workshop\Testing\Legacy;

use Dibi\Connection;

class dbz
{
    /** @var Connection */
    private static $connection;

    /**
     * @param Connection $connection
     */
    public static function init(Connection $connection)
    {
        static::$connection = $connection;
    }

    /**
     * Delete all units
     */
    public static function deleteUnits()
    {
        static::$connection->query('delete * from unit')->execute();
    }
}

