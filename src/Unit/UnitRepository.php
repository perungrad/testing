<?php

namespace Workshop\Testing\Unit;

use Dibi\Connection;

class UnitRepository
{
    /** @var Connection */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $query = $this->connection->select('id, name, type')
            ->select('attack, defense, speed')
            ->from('unit')
            ->orderBy('type, id');

        $result = $query->execute();

        return $result->fetchAssoc('id');
    }
}

