module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ['www/assets'],
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: ['assets/**'],
                        dest: 'www/'
                    },
                    {
                        expand: true,
                        cwd: 'assets/vendor/bootstrap-sass/assets/fonts/bootstrap',
                        src: ['*'],
                        dest: 'www/assets/fonts/bootstrap/'
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Default task(s).
    grunt.registerTask('default', ['clean', 'copy']);

};
